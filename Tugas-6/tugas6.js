//Soal 1

const luasLingkaran = (pi, r) => {
    let hasil = pi * r * r;
    return hasil;
}
console.log(luasLingkaran(22/7, 7));

const KLingkaran = (pi, r) => {
    let hasil = pi * r * 2;
    return hasil;
}
console.log(KLingkaran(22/7, 7));

//Soal 2

const kata1 = 'saya'
const kata2 = 'adalah'
const kata3 = 'seorang'
const kata4 = 'frontend'
const kata5 = 'developer'
 
const kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
 
console.log(kalimat)

//Soal 3


const firstName = "firstName"
const lastName = 'lastName'
const newFunction = `${firstName} ${lastName}`
 
console.log(newFunction)

//Soal 4

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName, lastName, destination, occupation} = newObject

  console.log(firstName, lastName, destination, occupation)

//Soal 5

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

console.log(combined)